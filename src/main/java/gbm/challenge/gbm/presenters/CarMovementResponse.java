package gbm.challenge.gbm.presenters;

public class CarMovementResponse {
	private Integer numeroError;
	private String	mensajeError;
	
	public Integer getNumeroError() {
		return numeroError;
	}
	public void setNumeroError(Integer numeroError) {
		this.numeroError = numeroError;
	}
	public String getMensajeError() {
		return mensajeError;
	}
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
}
