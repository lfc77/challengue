package gbm.challenge.gbm.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import gbm.challenge.gbm.gateway.RegisterCarRepository;
import gbm.challenge.gbm.presenters.CarMovementRequest;

@RestController
@RequestMapping("/car/position")
public class CarNavigationController{
	@Autowired
	RegisterCarRepository registerCarRepository;
	
	@GetMapping("/search/{carID}")
	public CarMovementRequest getCarPosition(@PathVariable(value="carID") Integer carID) {
		System.out.print("ID del carro a buscar "+carID);
		CarMovementRequest carMovement = new CarMovementRequest();
		carMovement = registerCarRepository.findById(carID).orElse(new CarMovementRequest());
		
		System.out.println(carMovement.getCarID());
		return carMovement;
	}
}
