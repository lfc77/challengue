package gbm.challenge.gbm.gateway;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import gbm.challenge.gbm.presenters.CarMovementRequest;
import gbm.challenge.gbm.presenters.CarMovementResponse;
import gbm.challenge.gbm.services.RegisterService;

@Service
public class RegisterImpl implements RegisterService{
	
	@Autowired
	RegisterCarRepository registerCarRepository;
	
	public CarMovementResponse registerCar(CarMovementRequest carMovementRequest) {
		registerCarRepository.save(carMovementRequest);
		CarMovementResponse carMovementResponse = new CarMovementResponse();
		
		carMovementResponse.setNumeroError(0);
		carMovementResponse.setMensajeError("Navegacion registrada");
		
		return carMovementResponse;
	}

}
