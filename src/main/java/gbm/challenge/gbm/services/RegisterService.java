package gbm.challenge.gbm.services;

import gbm.challenge.gbm.presenters.CarMovementRequest;
import gbm.challenge.gbm.presenters.CarMovementResponse;

public interface RegisterService {
	public CarMovementResponse registerCar(CarMovementRequest carMovementRequest);
}
