create table if not exists carNavigation(
	carID		int(12) primary key,
	latitude	varchar(20),
	attitude	varchar(20)
);